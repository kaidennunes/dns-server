#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<unistd.h>
#include<time.h> 

typedef unsigned int dns_rr_ttl;
typedef unsigned short dns_rr_type;
typedef unsigned short dns_rr_class;
typedef unsigned short dns_rdata_len;
typedef unsigned short dns_rr_count;
typedef unsigned short dns_query_id;
typedef unsigned short dns_flags;

typedef struct
{
	char *name;
	dns_rr_type type;
	dns_rr_class class;
	dns_rr_ttl ttl;
	dns_rdata_len rdata_len;
	unsigned char *rdata;
} dns_rr;

void print_bytes(unsigned char *bytes, int byteslen)
{
	int i, j, byteslen_adjusted;
	unsigned char c;

	if (byteslen % 8)
	{
		byteslen_adjusted = ((byteslen / 8) + 1) * 8;
	}
	else
	{
		byteslen_adjusted = byteslen;
	}
	for (i = 0; i < byteslen_adjusted + 1; i++)
	{
		if (!(i % 8))
		{
			if (i > 0)
			{
				for (j = i - 8; j < i; j++)
				{
					if (j >= byteslen_adjusted)
					{
						printf("  ");
					}
					else if (j >= byteslen)
					{
						printf("  ");
					}
					else if (bytes[j] >= '!' && bytes[j] <= '~')
					{
						printf(" %c", bytes[j]);
					}
					else
					{
						printf(" .");
					}
				}
			}
			if (i < byteslen_adjusted)
			{
				printf("\n%02X: ", i);
			}
		}
		else if (!(i % 4))
		{
			printf(" ");
		}
		if (i >= byteslen_adjusted)
		{
			continue;
		}
		else if (i >= byteslen)
		{
			printf("   ");
		}
		else
		{
			printf("%02X ", bytes[i]);
		}
	}
	printf("\n");
}

/*
* Canonicalize name in place.  Change all upper-case characters to
* lower case and remove the trailing dot if there is any.  If the name
* passed is a single dot, "." (representing the root zone), then it
* should stay the same.
*
* INPUT:  name: the domain name that should be canonicalized in place
*/
void canonicalize_name(char *name)
{
	int namelen, i;

	// leave the root zone alone
	if (strcmp(name, ".") == 0)
	{
		return;
	}

	namelen = strlen(name);
	// remove the trailing dot, if any
	if (name[namelen - 1] == '.')
	{
		name[namelen - 1] = '\0';
	}

	// make all upper-case letters lower case
	for (i = 0; i < namelen; i++)
	{
		if (name[i] >= 'A' && name[i] <= 'Z')
		{
			name[i] += 32;
		}
	}
}

/*
* Convert a DNS name from string representation (dot-separated labels)
* to DNS wire format, using the provided byte array (wire).  Return
* the number of bytes used by the name in wire format.
*
* INPUT:  name: the string containing the domain name
* INPUT:  wire: a pointer to the array of bytes where the
*              wire-formatted name should be constructed
* OUTPUT: the length of the wire-formatted name.
*/
int name_ascii_to_wire(char *name, unsigned char **wire)
{
	/* Calculate how many bytes we will need */
	unsigned int encoded_name_byte_length = strlen(name) + 2;

	/* Allocate memory for the array of bytes */
	*wire = (char *)malloc(encoded_name_byte_length);

	unsigned int label_length = 0;
	for (unsigned int i = 0; i < strlen(name); i++)
	{
		/* If we have a period separater, then skip it and start a new label */
		if (name[i] == '.')
		{
			(*wire)[i - label_length] = label_length;
			label_length = 0;
			continue;
		}
		(*wire)[i + 1] = name[i];
		label_length++;
	}
	/* Put in the last label length markers*/
	(*wire)[strlen(name) - label_length] = label_length;
	(*wire)[strlen(name) + 1] = 0;

	return encoded_name_byte_length;
}

/*
* Extract the wire-formatted DNS name at the offset specified by
* *indexp in the array of bytes provided (wire) and return its string
* representation (dot-separated labels) in a char array allocated for
* that purpose.  Update the value pointed to by indexp to the next
* value beyond the name.
*
* INPUT:  wire: a pointer to an array of bytes
* INPUT:  indexp, a pointer to the index in the wire where the
*              wire-formatted name begins
* OUTPUT: a string containing the string representation of the name,
*              allocated on the heap.
*/
char *name_ascii_from_wire(unsigned char *wire, int *indexp)
{
	int label_length = -1;
	unsigned int char_past_label = 0;
	unsigned int name__byte_size = 0;

	/* A domain may have only up to 256 char, including separators */
	unsigned char name[256];

	while (1)
	{
		/* Get the length of the next label */
		label_length = wire[(*indexp)++];

		/* If the length is 0, that means we are done parsing the name out */
		if (label_length == 0)
		{
			break;
		}

		/* Otherwise, add a period separator (if this isn't the beginning of the parsing of the name) */
		if (name__byte_size > 0)
		{
			name[name__byte_size++] = '.';
		}

		/* No label can be greater than or equal to 192. If it is, then we know that it is a pointer (compressed) */
		if (label_length >= 192)
		{
			/* Allocate memory for another pointer for our recursive call */
			unsigned int *name_pointer = malloc(sizeof(int));
			*name_pointer = wire[(*indexp)++];

			/* Get the portion of the name from that pointer reference using recursion */
			unsigned char *value_from_pointer = name_ascii_from_wire(wire, name_pointer);

			/* Free up the extra index pointer we allocated memory to*/
			free(name_pointer);

			/* Copy over the data from the portion of the name we just found to the current name we had found already */
			for (unsigned int i = 0; i < strlen(value_from_pointer); i++)
			{
				name[name__byte_size++] = value_from_pointer[i];
			}

			/* Free up the memory used from that recursive call */
			free(value_from_pointer);
			break;
		}

		/* Add the next characters to the name, using the label length to know how much to add */
		for (unsigned int i = 0; i < label_length; i++)
		{
			name[name__byte_size++] = wire[(*indexp)++];
		}
	}

	/* Once we are done finding the name, allocate memory to hold that name so we can return it, using only as much memory as necessary. Then copy over the contents to the new pointer */
	unsigned char *full_name = (char *)malloc(name__byte_size + 1);
	for (unsigned int i = 0; i < name__byte_size; i++)
	{
		full_name[i] = name[i];
	}

	/* Add the null terminator to the string */
	full_name[name__byte_size] = '\0';

	return full_name;
}

/*
* Extract the wire-formatted resource record at the offset specified by
* *indexp in the array of bytes provided (wire) and return a
* dns_rr (struct) populated with its contents. Update the value
* pointed to by indexp to the next value beyond the resource record.
*
* INPUT:  wire: a pointer to an array of bytes
* INPUT:  indexp: a pointer to the index in the wire where the
*              wire-formatted resource record begins
* INPUT:  query_only: a boolean value (1 or 0) which indicates whether
*              we are extracting a full resource record or only a
*              query (i.e., in the question section of the DNS
*              message).  In the case of the latter, the ttl,
*              rdata_len, and rdata are skipped.
* OUTPUT: the resource record (struct)
*/
dns_rr rr_from_wire(unsigned char *wire, int *indexp, int query_only)
{
	/* Set the the resource record memory */
	dns_rr resource_record;

	/* Get the name for that resource record */
	resource_record.name = name_ascii_from_wire(wire, indexp);

	/* Get the type of that resource record */
	resource_record.type = (wire[*indexp] << 8) | (wire[*indexp + 1] & 0xFF);
	*indexp += 2;

	/* Get the type of that resource record */
	resource_record.class = (wire[*indexp] << 8) | (wire[*indexp + 1] & 0xFF);
	*indexp += 2;

	/* If theis is a query only conversion, then skip this part*/
	if (query_only == 0)
	{
		/* Get the ttl of that resource record */
		resource_record.ttl = wire[*indexp] << 24 | ((int)wire[*indexp + 1] << 16) | ((int)wire[*indexp + 2] << 8) | ((int)wire[*indexp + 3]);
		*indexp += 4;

		/* Get the length of that resource record's rdata */
		resource_record.rdata_len = (wire[*indexp] << 8) | (wire[*indexp + 1] & 0xFF);
		*indexp += 2;

		/* If that resource record's type is canonical, then the rdata is going to be the canonical name, so parse that out */
		if (resource_record.type == 5)
		{
			resource_record.rdata = name_ascii_from_wire(wire, indexp);
		}
		else
		{
			// Otherwise, the data is an IP address, extract it
			char *data = (char *)malloc(resource_record.rdata_len * 4);
			sprintf(data, "%hhu.%hhu.%hhu.%hhu", wire[*indexp], wire[*indexp + 1], wire[*indexp + 2], wire[*indexp + 3]);
			*indexp += 4;

			resource_record.rdata = data;
		}
	}

	return resource_record;
}

/*
* Create a wire-formatted DNS (query) message using the provided byte
* array (wire).  Create the header and question sections, including
* the qname and qtype.
*
* INPUT:  qname: the string containing the name to be queried
* INPUT:  qtype: the integer representation of type of the query (type A == 1)
* INPUT:  wire: the pointer to the array of bytes where the DNS wire
*               message should be constructed
* OUTPUT: the length of the DNS wire message
*/
unsigned short create_dns_query(char *qname, dns_rr_type qtype, unsigned char **wire)
{
	/* Encode the name to the correct format */
	unsigned char *encoded_name;
	unsigned int encoded_name_length = name_ascii_to_wire(qname, &encoded_name);

	/* The size of the dns query header */
	unsigned int dns_header_byte_size = 12;

	/* The size of the question ending*/
	unsigned int question_ending_byte_size = 4;

	/* Get the total size of the query */
	unsigned int message_byte_size = dns_header_byte_size + encoded_name_length + question_ending_byte_size;

	/* Allocate the necessary memory for the query */
	*wire = (char *)malloc(message_byte_size);

	/* Set the dns header bits to the proper values (random ID, query status, recursion, etc.) */
	(*wire)[0] = (unsigned short)htons(rand());
	(*wire)[1] = (unsigned short)htons(rand());
	(*wire)[2] = 0x01;
	(*wire)[3] = 0x00;
	(*wire)[4] = 0x00;
	(*wire)[5] = 0x01;
	(*wire)[6] = 0x00;
	(*wire)[7] = 0x00;
	(*wire)[8] = 0x00;
	(*wire)[9] = 0x00;
	(*wire)[10] = 0x00;
	(*wire)[11] = 0x00;
	(*wire)[message_byte_size - 4] = 0x00;
	(*wire)[message_byte_size - 3] = 0x01;
	(*wire)[message_byte_size - 2] = 0x00;
	(*wire)[message_byte_size - 1] = 0x01;

	/* Add to the dns query the domain name we want to get the IP addresses for */
	for (unsigned int i = 0; i < encoded_name_length; i++)
	{
		(*wire)[i + dns_header_byte_size] = encoded_name[i];
	}

	/* Free the space allocated in the function */
	free(encoded_name);

	return message_byte_size;
}

/*
* Extract the IPv4 address from the answer section, following any
* aliases that might be found, and return the string representation of
* the IP address.  If no address is found, then return NULL.
*
* INPUT:  qname: the string containing the name that was queried
* INPUT:  qtype: the integer representation of type of the query (type A == 1)
* INPUT:  wire: the pointer to the array of bytes representing the DNS wire message
* OUTPUT: a string representing the IP address in the answer; or NULL if none is found
*/
int get_answer_address(char *qname, dns_rr_type qtype, unsigned char *wire, unsigned char ***answers, unsigned char **actual_qname)
{
	/* This is where the beginning of the stuff we are interesting are at */
	unsigned int start_byte_index = 6;

	/* Find the number of resource records */
	unsigned int number_resource_records = (wire[start_byte_index] << 8) | (wire[start_byte_index + 1] & 0xFF);

	/* This will hold the final actual canonical name */
	*actual_qname = (unsigned char *)malloc((strlen(qname) + 1));
	strcpy((*actual_qname), qname);

	if (number_resource_records == 0)
	{
		return 0;
	}

	/* This is where the beginning of the questions is at */
	unsigned int *question_byte_index = malloc(sizeof(int));
	*question_byte_index = 12;

	/* This will hold all of our answers */
	*answers = malloc(number_resource_records * sizeof(char*));

	/* Go through the question record (to advance the index pointer to the correct location), then free up the memory afterwards, since we don't care about the question record */
	dns_rr question_record = rr_from_wire(wire, question_byte_index, 1);
	free(question_record.name);

	/* Go through all of the resource records and add the IP addresses to the array of answers (and keep track of the canonical name) */
	unsigned int number_answer_resource_records_used = 0;
	for (unsigned int i = 0; i < number_resource_records; i++)
	{
		/* Parse out the resource record */
		dns_rr resource_record = rr_from_wire(wire, question_byte_index, 0);

		/* See if the owner name of RR matches qname and the type matches the qtype */
		if (!strcmp(*actual_qname, resource_record.name) && qtype == resource_record.type)
		{
			/* Extract the address from the RR, convert it to a string, and add it to the result list */
			(*answers)[number_answer_resource_records_used++] = resource_record.rdata;
		}
		else if (!strcmp(*actual_qname, resource_record.name) && resource_record.type == 5)
		{
			/* Free up the resources from where the qname pointed to before*/
			free(*actual_qname);

			/* Set qname to the canonical name from the resource record's rdata */
			*actual_qname = (unsigned char *)malloc((strlen(resource_record.rdata) + 1));
			strcpy((*actual_qname), resource_record.rdata);

			/* Free up that resource record's rdata because it won't be freed up later */
			free(resource_record.rdata);
		}

		// Free up that resource record's resources name
		free(resource_record.name);
	}

	// Free up the int pointer we were using
	free(question_byte_index);

	return number_answer_resource_records_used;
}

/*
* Send a message (request) over UDP to a server (server) and port
* (port) and wait for a response, which is placed in another byte
* array (response).  Create a socket, "connect()" it to the
* appropriate destination, and then use send() and recv();
*
* INPUT:  request: a pointer to an array of bytes that should be sent
* INPUT:  requestlen: the length of request, in bytes.
* INPUT:  response: a pointer to an array of bytes in which the
*             response should be received
* OUTPUT: the size (bytes) of the response received
*/
int send_recv_message(unsigned char *request, int requestlen, unsigned char **response, char *server, unsigned short port)
{
	unsigned int buffer_size = 512;
	char buffer[buffer_size];

	struct sockaddr_in destination;

	int server_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	if (server_socket == -1)
	{
		printf("\nServer socket creation failed. Try again later.\n");
		return -1;
	}

	destination.sin_family = AF_INET;
	destination.sin_port = htons(port);
	destination.sin_addr.s_addr = inet_addr(server);


	if (connect(server_socket, (struct sockaddr *)&destination, sizeof(struct sockaddr_in)) == -1)
	{
		printf("\nConnection failed. Try again later.\n");
		return -1;
	}

	if (send(server_socket, request, requestlen, 0) == -1)
	{
		printf("\nSending request failed. Try again later.\n");
		return -1;
	}

	unsigned int query_response_length = recv(server_socket, buffer, buffer_size, 0);

	*response = (char *)malloc(query_response_length);

	for (unsigned int i = 0; i < query_response_length; i++)
	{
		(*response)[i] = buffer[i];
	}

	close(server_socket);

	return query_response_length;
}

int resolve(char *qname, char *server, unsigned char ***ip_addresses, unsigned char **actual_qname)
{
	/* Canonicalize the domain name */
	canonicalize_name(qname);

	/* Create the query. The 1 is the type, in this case, always the address type.*/
	unsigned char *query_message;
	int query_length = create_dns_query(qname, 1, &query_message);

	/* Send the query */
	unsigned char *query_response;
	int response_length = send_recv_message(query_message, query_length, &query_response, server, 53);

	/* Free the space allocated in the create query function */
	free(query_message);

	/* Quit if the response length is invalid (and free up the space) */
	if (response_length == -1)
	{
		return response_length;
	}

	/* Get and return the IP address */
	unsigned int number_of_answers = get_answer_address(qname, 1, query_response, ip_addresses, actual_qname);

	/* Free the space allocated in the send message function */
	free(query_response);

	return number_of_answers;
}

int main(int argc, char *argv[])
{
	/* Initialize the random element, as well as declare the pointer variables we will need*/
	srand(time(0));
	unsigned char **ip_addresses;
	unsigned char *actual_qname;
	if (argc < 3)
	{
		fprintf(stderr, "Usage: %s <domain name> <server>\n", argv[0]);
		exit(1);
	}

	/* Get the IP addresses for that domain name */
	unsigned int number_of_answers = resolve(argv[1], argv[2], &ip_addresses, &actual_qname);

	/* If there was an error, report that */
	if (number_of_answers == -1)
	{
		return -1;
	}

	/* If there are no IP addresses for that domain name, indicate that, free up the memory, then quit */
	if (number_of_answers == 0)
	{
		printf("%s => NONE\n", actual_qname);
		free(actual_qname);
		return 0;
	}

	/* Otherwise, print out all the IP addresses, freeing up the memory as we print them out */
	for (unsigned int i = 0; i < number_of_answers; i++)
	{
		printf("%s => %s\n", actual_qname, ip_addresses[i]);
		free(ip_addresses[i]);
	}

	/* Free the space allocated in the function */
	free(ip_addresses);
	free(actual_qname);
}
